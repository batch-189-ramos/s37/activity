const express = require('express');
const router = express.Router();
const courseController = require('../controllers/courseController');
const auth = require('../auth')



router.post("/", auth.verify, (req, res) => {

	const isAdminData = auth.decode(req.headers.authorization).isAdmin;
	console.log(isAdminData);

	courseController.addCourse(req.body, isAdminData).then(resultFromController => res.send(resultFromController))
});


// Route for retrieving all courses
router.get("/all", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)
	courseController.getAllCourses(userData).then(resultFromController => res.send(resultFromController))
})

// Retrieving active courses-route
router.get("/", (req, res) => {
	courseController.getAllActive().then(resultFromController => res.send(resultFromController))
})

// Route for retrieving a specific course
// localhost:4000/courses/12541356a7115513 - _id
router.get("/:courseId", (req, res) => {
	console.log(req.params)
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController))
})

// Route for updating a course

router.put("/:courseId", auth.verify, (req, res) => {
	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
})



// Route for archiving a course

router.put("/toArchive/:courseId", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)
	console.log(userData)
	courseController.archiveCourse(req.params, req.body, userData).then(resultFromController => res.send(resultFromController))
})


module.exports = router;