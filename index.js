const express = require('express');
const mongoose = require('mongoose');
// cors allows our backend application to be available to our frontend application
const cors = require('cors');
const userRoutes = require('./routes/userRoutes');
const courseRoutes = require('./routes/courseRoutes');
const port = 3000;

const app = express();

// Allows all resources to access our backend application
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use("/users", userRoutes);
app.use("/courses", courseRoutes);

mongoose.connect("mongodb+srv://RuRuSensei:admin123@zuitt-bootcamp.5jpfbgk.mongodb.net/S37-S41?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;
db.on('error', () => console.error.bind(console, 'error'));
db.once('open', () => console.log('Now connected to MongoDb Atlas'));

app.listen(process.env.PORT || port, () => {
	console.log(`API is now online on port ${process.env.PORT || port}`);
})

