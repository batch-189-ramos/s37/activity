const Course = require('../models/Course');
const bcrypt = require('bcrypt');
const auth = require('../auth');

// Create a new course

module.exports.addCourse = (reqBody, isAdminData) => {
	if(isAdminData == true){
	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	});

	return newCourse.save().then((course, error) => {

		if(error) {
			return false
		} else {
			return true
		}
	})
}}


// Retrieve All Courses
module.exports.getAllCourses = async (data) => {

	if (data.isAdmin) {
		return Course.find({}).then(result => {
		return result
		})
	} else {
		return "Not an Admin"
	}
}


module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(result => {
		return result
	})
}

module.exports.getCourse = (reqParams) => {

	return Course.findById(reqParams.courseId).then(result => {
		return result
	})
} 

module.exports.updateCourse = (reqParams, reqBody) => {
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
		if (error) {
			return false
		} else {
			return true
		}
	})
}

module.exports.archiveCourse = async (reqParams, reqBody, userData) => {

	if(userData.isAdmin == true) {
		let archiveCourse = {
			isActive: reqBody.isActive
		}
		return Course.findByIdAndUpdate(reqParams.courseId, archiveCourse).then((course, error) => {
			if(error) {
				return false
			} else {
				return true
			}
		})
	} else {
		return "Not an Admin"
	}

}